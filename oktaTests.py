from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import unittest
import time

class oktaTest(unittest.TestCase):

    oktaUrl = 'https://dev-201588-admin.oktapreview.com'
    oktaUsername = 'jyu@atlassian.com'
    oktaPassword = 'Scientia989' #yes I know I'll delete it later. No it isn't used for any of my other passwords.

    adminHubUrl = 'https://admin.atlassian.com/'
    atlUsername = 'idptest@climbtrainer.com'
    atlPassword = 'atlassian'

    oktaSingleUserFirstname = 'Frodo'
    oktaSingleUserLastname = 'Baggins'
    oktaSingleUserEmail = 'FrodoBaggins@climbtrainer.com'

    driver = None
    token = None
    baseUrl = None

    def setUp(self):
        self.driver = webdriver.Safari()
        self.driver.maximize_window()
        self.doAtlLogin()
        self.regenerateSCIMToken()
        time.sleep(1)

    def tearDown(self):
        self.driver.get(self.oktaUrl)
        #okta delete token
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'nav-admin-apps'))
        ).click()
        time.sleep(3)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, 'Atlassian Cloud'))
        ).click()
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="tabs"]/ul/li[3]/a'))
        ).click()
        time.sleep(2)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="tab-user-management-"]/div[1]/div/div[1]/div/ul/li[3]/a'))
        ).click()
        time.sleep(2)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'userMgmtSettings.edit_link'))
        ).click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'userMgmtSettings.enabled'))
        ).click()
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'userMgmtSettings.button.submit'))
        ).click()
        time.sleep(2)

        #atlassian delete token
        self.driver.get(self.adminHubUrl)
        userProvisioningPage = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((
                By.XPATH, 
                '//*[@id="root"]/div/div[1]/div/div/div/div[1]/div/div[1]/div/div[2]/div/div[3]/div/div/div/div/div[1]/a[2]'
                ))
        )
        userProvisioningPage.click()

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((
                By.XPATH, 
                '//*[@id="root"]/div/div[1]/div/div/div/div[2]/span/div/div/div/div/div[3]/div[2]/div/div[1]/div/div[4]'
                ))
        ).click()

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'scim-remove-directory-button'))
        ).click()

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'scim-confirm-remove-directory-button'))
        ).click()
        #wait for success popup
        self.driver.quit()

    def test_user_sync(self):
        self.doOktaLogin()
        time.sleep(3)
        self.setUpOktaProvisioning()
        #go to user page
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'nav-admin-people'))
        ).click()
        self.generateOktaTestUser()

        time.sleep(60)

        self.driver.get(self.adminHubUrl)
        #check managed user tab and verify
        time.sleep(15)

        self.assertTrue(self.checkTestUserExists())

        #-----single user cleanup---------

        #go back to okta and delete the okta test user
        self.driver.get(self.oktaUrl)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'nav-admin-people'))
        ).click()
        time.sleep(2)

        #select the specific test user name, click
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="people-page-container"]/div/div[2]/div[3]/div/table/tbody[2]/tr/td[1]/a'))
        ).click()
        time.sleep(2)

        #click dropdown
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="people-more-actions-dropdown"]/a'))
        ).click()        
        #click deactivate
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="deactivate-person-dropdown-option"]/a'))
        ).click()
        #click confirm
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'user-deactivation-confirmed'))
        ).click()     
        #click delete
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'delete-user'))
        ).click()  
        #click confirm
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//input[@class='button button-error' and @type='submit' and @value='Delete']"))
        ).click()  
        #wait for confirmation
        time.sleep(3)

        print("test user sync placeholder")

    # def test_group_sync(self):
    #     print("test group sync placeholder")

    # def test_group_removal(self):
    #     print("test group removal placeholder")
    
    # def test_ignore_group_rename(self):
    #     print("test ignore group rename placeholder")

    #--------helpers--------

    def doAtlLogin(self):
        driver = self.driver
        driver.get(self.adminHubUrl)
        emailBox = driver.find_element(By.ID, "username")
        emailBox.send_keys(self.atlUsername)
        submitButton = driver.find_element(By.ID, "login-submit")
        submitButton.click()
        time.sleep(1)
        passwordBox = WebDriverWait(driver, 250).until(
            EC.presence_of_element_located((By.ID, "password"))
        )
        passwordBox.send_keys(self.atlPassword)
        submitButton.click()

    def doOktaLogin(self):
        driver = self.driver
        driver.get(self.oktaUrl)
        usernameBox = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((
                By.ID, 
                'okta-signin-username'
                ))
        )
        usernameBox.send_keys(self.oktaUsername)
        passwordBox = driver.find_element(By.ID, "okta-signin-password")
        passwordBox.send_keys(self.oktaPassword)
        submitButton = driver.find_element(By.ID, "okta-signin-submit")
        submitButton.click()
        print("doOktaLogin WIP")

    def checkTestUserExists(self):
        try:
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//span[@title="Frodo Baggins"]'))
            )
        except NoSuchElementException:
            return False
        try:
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//span[@title="frodobaggins@climbtrainer.com"]'))
            )
        except NoSuchElementException:
            return False
        return True

    def setUpOktaProvisioning(self):
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'nav-admin-apps'))
        ).click()
        time.sleep(3)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, 'Atlassian Cloud'))
        ).click()
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="tabs"]/ul/li[3]/a'))
        ).click()
        time.sleep(2)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, 'Configure API Integration'))
        ).click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'userMgmtSettings.enabled'))
        ).click()
        baseUrlBox = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'userMgmtSettings.scim_base_url'))
        )
        baseUrlBox.clear()
        baseUrlBox.send_keys(self.baseUrl)
        tokenBox = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'userMgmtSettings.scim_auth_header_value'))
        )
        tokenBox.clear()
        tokenBox.send_keys(self.token)
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'm-verify-button'))
        ).click()
        time.sleep(3)
        #expect to find div id userMgmtSettings.validation.success

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'userMgmtSettings.button.submit'))
        ).click()
        time.sleep(4)

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'userMgmtSettings.edit_link'))
        ).click()
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'userMgmtSettings.pushNewAccount'))
        ).click()
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'userMgmtSettings.button.submit'))
        ).click()

        time.sleep(3)

    def generateOktaTestUser(self):
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'add-user'))
        ).click()
        firstNameBox = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.NAME, 'profile.firstName'))
        )
        firstNameBox.send_keys(self.oktaSingleUserFirstname)
        lastNameBox = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.NAME, 'profile.lastName'))
        )
        lastNameBox.send_keys(self.oktaSingleUserLastname)
        emailUsernameBox = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.NAME, 'profile.login'))
        )
        emailUsernameBox.send_keys(self.oktaSingleUserEmail)
        primaryEmailBox = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.NAME, 'profile.email'))
        )
        primaryEmailBox.send_keys(self.oktaSingleUserEmail)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//input[@class='button button-primary' and @type='submit']")) #submit
        ).click()

        time.sleep(3)

        #click in on the user we just made
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, '        ' + self.oktaSingleUserFirstname))
        ).click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, 'assign-edit-app-button'))
        ).click()
        time.sleep(1)
        #assign atlassian cloud app
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="simplemodal-data"]/div/div[2]/div/table/tbody/tr/td[3]/input'))
        ).click()
        #save
        time.sleep(3)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="form187"]/div[2]/input[1]'))
        ).click()
        
        print("generate user WIP")

    def regenerateSCIMToken(self):
        userProvisioningTab = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((
                By.XPATH, 
                '//*[@id="root"]/div/div/div/div/div/div[1]/div/div[1]/div/div[2]/div/div[3]/div/div/div/div/div[1]/a[2]'
                ))
        )
        userProvisioningTab.click()

        createDirectoryButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((
                By.XPATH, 
                '//*[@id="root"]/div/div/div/div/div/div[2]/div/div[2]/div[2]/div/div/div[3]/button'
                ))
        )
        createDirectoryButton.click()

        directoryNameField = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((
                By.ID, 
                'scim-create-directory-name'
                ))
        )
        directoryNameField.send_keys('testDirectory')
        self.driver.find_element_by_id('scim-create-directory-button').click()

        #save baseUrl
        baseUrlField = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((
                By.XPATH, 
                '//*[@id="root"]/div/div/div/div/div/div[1]/div/div[4]/div[2]/div[2]/div[2]/div/div[3]/div[1]/div/div/div/div/input'
                ))
        )
        self.baseUrl = baseUrlField.get_attribute('value')

        #save token
        tokenField = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((
                By.XPATH, 
                '//*[@id="root"]/div/div/div/div/div/div[1]/div/div[4]/div[2]/div[2]/div[2]/div/div[4]/div[1]/div/div/div/div/input'
                ))
        )
        self.token = tokenField.get_attribute('value')

        self.driver.find_element_by_id('scim-api-key-details-done-button').click()
        print("regenerate ATL SCIM token WIP")
    
    def cleanup(self):
        print("cleanup placeholder")

if __name__ == '__main__':
    unittest.main()
